*** Settings ***
Documentation       Common Steps
Library             SeleniumLibrary

*** Variables ***
${landind_page}         http://the-internet.herokuapp.com/


*** Keywords ***
Test Setup Actions
    ${options} =    Evaluate    selenium.webdriver.ChromeOptions()
    Call Method    ${options}    add_argument    --log-level\=3
    Call Method    ${options}    add_argument    --size
    Call Method    ${options}    add_argument    --window-size\=1920,1080
    Call Method    ${options}    add_argument    --headless
    Call Method    ${options}    add_argument    --no-sandbox
    Call Method    ${options}    add_argument    --disable-dev-shm-usage
    Call Method    ${options}    add_argument    --disable-extensions
    Open Browser   about:blank  headlesschrome  alias=singleTone    options=${options}
    Maximize Browser Window
    Go To                           ${landind_page}

Suite Teardown Actions
    Close All Browsers




#*** Settings ***
#Documentation       Common Steps
#Library             SeleniumLibrary
#
#*** Variables ***
#${landind_page}         http://the-internet.herokuapp.com/
#
#
#*** Keywords ***
#
#Create WebDriver With Chrome Options
#    #${chrome_options} =    Evaluate    selenium.webdriver.ChromeOptions()
#    ${options}  Evaluate    sys.modules['selenium.webdriver'].ChromeOptions
#    #Call Method    ${chrome_options}    add_argument    --no-sandbox
#    Call Method    ${options}    add_argument    --disable-dev-shm-usage
#    Call Method    ${options}    add_argument    --no-sandbox
#    Call Method    ${options}    add_argument    --headless
#   # Call Method    ${chrome_options}    add_argument    --incognito
#   # Call Method    ${chrome_options}    add_argument    --disable-extensions
#    Create WebDriver    Chrome    options=${Options}
#    Go To  http://the-internet.herokuapp.com/  Chrome
#
#Test Setup Actions
#    #Open Browser                    about:blank  chrome
#    #Open Browser                    about:blank
#   # Maximize Browser Window
#    Go To                           ${landind_page}
#
#Suite Teardown Actions
#    Close All Browsers
